#include <iostream>
#include <fstream>
#include <chrono>
#include <vector>
#include <algorithm>

using namespace std;
using namespace std::chrono;

// Globalne spremenljivke
int N;
vector<int> polje1;
vector<int> polje2;

bool preberiPodatke(const char s[]) {

	ifstream input(s);
	int a;

	if (!input.is_open()) { // Ce file ne obstaja
		cout << "Datoteka s tem imenom ne obstaja." << endl;
		return false;
	}

	input >> N; // Preberemo N 
	for (int i = 0; i < N; i++) { // Preberemo prvih N elementov
		input >> a;
		polje1.push_back(a);
	}
	for (int i = 0; i < N; i++) { // Preberemo drugih N elementov
		input >> a;
		polje2.push_back(a);
	}
	return true;
}

void odstraniUtezi(int y, vector<int> &polje3, vector<int> &polje4) {

	for (int i = 0; i < polje1.size(); i++) {
		if (polje1[i] > y) {
			polje3.push_back(polje1[i]);
		}
		if (polje2[i] > y) {
			polje4.push_back(polje2[i]);
		}
	}

	// Re�itev z odstranjanjem elementov, a je zelo po�re�na
	/*int x = polje3.size() - 1;
	for (int i = x; i >= 0; i--) {
		if (polje3[i] <= y) {
			polje3.erase(polje3.begin() + i);
		}
	}
	x = polje4.size() - 1;
	for (int i = x; i >= 0; i--) {
		if (polje4[i] <= y) {
			polje4.erase(polje4.begin() + i);
		}
	}*/
}

bool f(int y) {

	vector<int> polje3;
	vector<int> polje4;

	odstraniUtezi(y, polje3, polje4);

	if (polje3.empty() && polje4.empty()) {
		return true;
	}

	if (polje3.size() == 1 || polje4.size() == 1) {
		return false;
	}

	for (int i = 0; i < polje3.size(); i = i + 2) { // Se pomikamo za 2 naprej in gledamo �e sta enaka
		if (polje3[i] != polje3[i + 1]) {
			return false;
		}
	}

	for (int i = 0; i < polje4.size(); i = i + 2) {
		if (polje4[i] != polje4[i + 1]) {
			return false;
		}
	}
	return true;
}

bool preveriUrejenost() {

	for (int i = 0; i < polje1.size(); i = i + 2) {
		if (polje1[i] != polje1[i + 1]) {
			return false;
		}
	}

	for (int i = 0; i < polje2.size(); i = i + 2) {
		if (polje2[i] != polje2[i + 1]) {
			return false;
		}
	}

	return true;
}

int bisekcija(int a, int b, vector<int> polje1, vector<int> polje2) {

	if (preveriUrejenost()) { // Preverimo �e so ute�i �e urejene
		return 0;
	}

	bool fx;
	int x = (a + b) / 2;

	while (a != x) { // Dokler ni a = x
		fx = f(x);
		if (fx) {
			b = x;
		}
		else {
			a = x;
		}
		x = (a + b) / 2;
	}
	return b;
}

int findMin() {

	int min1 = *min_element(polje1.begin(), polje1.end());
	int min2 = *min_element(polje2.begin(), polje2.end());

	if (min2 < min1) {
		return min2;
	}
	return min1;
}

int findMax() {

	int max1 = *max_element(polje1.begin(), polje1.end());
	int max2 = *max_element(polje2.begin(), polje2.end());

	if (max2 > max1) {
		return max2;
	}
	return max1;
}

int main(int argc, const char* argv[]) {

	if (argc <= 1 || !preberiPodatke(argv[1])) {
		return 0;
	}

	if (N < 0) {
		cout << "Neustrezni vhodni podatki." << endl;
		return 0;
	}

	auto start = high_resolution_clock::now();
	cout << "RESITEV: " << bisekcija(findMin(), findMax(), polje1, polje2) << endl;
	auto stop = high_resolution_clock::now();

	auto trajanje = duration_cast<milliseconds>(stop - start);
	cout << "Time: " << trajanje.count() << " miliseconds" << endl;

	system("PAUSE");
	return 0;
}
